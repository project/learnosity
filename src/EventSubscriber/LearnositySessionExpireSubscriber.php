<?php

namespace Drupal\learnosity\EventSubscriber;

use Drupal\learnosity\Event\LearnosityAssessmentEvent;
use Drupal\learnosity\Event\LearnosityEvent;
use Drupal\learnosity\LearnositySessionHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class LearnosityEventsSubscriber.
 *
 * @package Drupal\learnosity\EventSubscriber
 */
class LearnositySessionExpireSubscriber implements EventSubscriberInterface {

  /**
   * The Content Completion Settings service.
   *
   * @var \Drupal\learnosity\LearnositySessionHandler
   */
  protected $sessionHandler;

  /**
   * LearnosityEventsSubscriber constructor.
   *
   * @param \Drupal\learnosity\LearnositySessionHandler $session_handler
   *   The learnosity session handler.
   */
  public function __construct(LearnositySessionHandler $session_handler) {
    $this->sessionHandler = $session_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      LearnosityAssessmentEvent::FINISHED_SUBMIT => 'expireSessions',
    ];
  }

  /**
   * Test finished subscribed event callback.
   *
   * @param \Drupal\learnosity\Event\LearnosityEvent $event
   *   The learnosity event.
   */
  public function expireSessions(LearnosityEvent $event) {
    $context = $event->getContext();

    // Expire any existing sessions.
    \Drupal::service('learnosity.session_handler')->expire($context['session_id']);
  }

}
