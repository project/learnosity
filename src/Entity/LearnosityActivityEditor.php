<?php

namespace Drupal\learnosity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Serialization\Yaml;

/**
 * Represents a learnosity activity editor.
 *
 * @ConfigEntityType(
 *   id = "learnosity_activity_editor",
 *   label = @Translation("Activity editor"),
 *   label_collection = @Translation("Activity editors"),
 *   label_singular = @Translation("activity editor"),
 *   label_plural = @Translation("activity editors"),
 *   label_count = @PluralTranslation(
 *     singular = "@count activity editor",
 *     plural = "@count activity editors",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\learnosity\LearnosityActivityEditorListBuilder",
 *     "access" = "Drupal\learnosity\LearnosityActivityEditorAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\learnosity\Form\LearnosityActivityEditorForm",
 *       "edit" = "Drupal\learnosity\Form\LearnosityActivityEditorForm",
 *       "delete" = "Drupal\learnosity\Form\LearnosityActivityEditorDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *     },
 *   },
 *   config_prefix = "learnosity_activity_editor",
 *   admin_permission = "administer learnosity activity editors",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "weight" = "weight",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/learnosity/editors/add",
 *     "edit-form" = "/admin/config/learnosity/editors/{learnosity_activity_editor}",
 *     "delete-form" = "/admin/config/learnosity/editors/{learnosity_activity_editor}/delete",
 *     "collection" = "/admin/config/learnosity/editors"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "weight",
 *     "config",
 *     "features",
 *     "upload_widget",
 *   }
 * )
 */
class LearnosityActivityEditor extends ConfigEntityBase {

  /**
   * Unique machine name of the activity label.
   *
   * @var string
   */
  protected $id;

  /**
   * The activity editor label.
   *
   * @var string
   */
  protected $label;

  /**
   * Weight of this format in the text format selector.
   *
   * The first/lowest text format that is accessible for a user is used as
   * default format.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * Config settings for the learnosity initialization.
   *
   * @var string
   */
  protected $config;

  /**
   * Custom enabled features.
   *
   * @var array
   */
  protected $features = [];

  /**
   * Upload widget.
   *
   * @var string
   */
  protected $upload_widget = [];

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfig($config) {
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig($decode = FALSE) {
    if ($decode) {
      return Yaml::decode($this->config);
    }
    return $this->config;
  }

  /**
   * {@inheritdoc}
   */
  public function setFeatures($features) {
    $this->features = $features;
  }

  /**
   * {@inheritdoc}
   */
  public function getFeatures() {
    return $this->features;
  }

  /**
   * {@inheritdoc}
   */
  public function setUploadWidget($widget) {
    $this->upload_widget = $widget;
  }

  /**
   * {@inheritdoc}
   */
  public function getUploadWidget() {
    return $this->upload_widget;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('weight');
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
  }

  /**
   * Determine whether this is the fallback editor.
   *
   * @return bool
   *   True if fallback FALSE otherwise.
   */
  public function isFallback() {
    $fallback = \Drupal::config('learnosity.settings')->get('fallback_editor');
    return $this->id() == $fallback;
  }

  /**
   * {@inheritdoc}
   */
  public function getPermissionName() {
    return 'use learnosity activity editor ' . $this->id();
  }

}
