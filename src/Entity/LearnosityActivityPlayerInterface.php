<?php

namespace Drupal\learnosity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Provides an interface for defining Activity type entities.
 */
interface LearnosityActivityPlayerInterface extends ConfigEntityInterface, EntityDescriptionInterface {

  /**
   * Gets the description.
   *
   * @return string
   *   The description of this activity type.
   */
  public function getDescription();

  /**
   * Sets the description.
   *
   * @param string $description
   *   The description of this activity type.
   */
  public function setDescription($description);

}
