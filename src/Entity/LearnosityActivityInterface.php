<?php

namespace Drupal\learnosity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Activity entities.
 *
 * @ingroup learnosity
 */
interface LearnosityActivityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Activity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Activity.
   */
  public function getCreatedTime();

  /**
   * Sets the Activity creation timestamp.
   *
   * @param int $timestamp
   *   The Activity creation timestamp.
   *
   * @return \Drupal\learnosity\Entity\LearnosityActivityInterface
   *   The called Activity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Activity published status indicator.
   *
   * Unpublished Activity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Activity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of an Activity.
   *
   * @param bool $published
   *   TRUE to set this Activity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\learnosity\Entity\LearnosityActivityInterface
   *   The called Activity entity.
   */
  public function setPublished($published);

  /**
   * Gets the reference of this activity.
   *
   * @return string
   *   The learnosity reference.
   */
  public function getReference();

  /**
   * Sets the reference of this activity.
   *
   * @param string $reference
   *   The activity's reference.
   */
  public function setReference($reference);

  /**
   * Gets the data of this activity.
   *
   * @return string
   *   The learnosity data.
   */
  public function getData();

  /**
   * Sets the data of this activity.
   *
   * @param array $data
   *   The activity's data.
   */
  public function setData(array $data);

  /**
   * Gets the Activity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Activity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\learnosity\Entity\LearnosityActivityInterface
   *   The called Activity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Activity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Activity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\learnosity\Entity\LearnosityActivityInterface
   *   The called Activity entity.
   */
  public function setRevisionUserId($uid);

}
