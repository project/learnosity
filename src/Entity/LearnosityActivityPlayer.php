<?php

namespace Drupal\learnosity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Serialization\Yaml;

/**
 * Defines the Activity type entity.
 *
 * @ConfigEntityType(
 *   id = "learnosity_activity_player",
 *   label = @Translation("Learnosity activity player"),
 *   handlers = {
 *     "list_builder" = "Drupal\learnosity\LearnosityActivityPlayerListBuilder",
 *     "form" = {
 *       "add" = "Drupal\learnosity\Form\LearnosityActivityPlayerForm",
 *       "edit" = "Drupal\learnosity\Form\LearnosityActivityPlayerForm",
 *       "delete" = "Drupal\learnosity\Form\LearnosityActivityPlayerDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\learnosity\LearnosityActivityPlayerHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "learnosity_activity_player",
 *   admin_permission = "administer learnosity activity players",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "canonical" = "/admin/config/learnosity/activities/{learnosity_activity_player}",
 *     "add-form" = "/admin/config/learnosity/activities/add",
 *     "edit-form" = "/admin/config/learnosity/activities/{learnosity_activity_player}",
 *     "delete-form" = "/admin/config/learnosity/activities/{learnosity_activity_player}/delete",
 *     "collection" = "/admin/config/learnosity/activities"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "config",
 *   }
 * )
 */
class LearnosityActivityPlayer extends ConfigEntityBase implements LearnosityActivityPlayerInterface {

  /**
   * The Activity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Activity type label.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this activity type.
   *
   * @var string
   */
  protected $description;

  /**
   * The Activity config.
   *
   * @var string
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfig($config) {
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig($decode = FALSE) {
    if ($decode) {
      return Yaml::decode($this->config);
    }
    return $this->config;
  }

}
