<?php

namespace Drupal\learnosity\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "learnosity_assessment_player",
 *   label = @Translation("Default"),
 *   description = @Translation("Display the referenced entities rendered by entity_view()."),
 *   field_types = {
 *     "learnosity_activity",
 *   }
 * )
 */
class LearnosityAssessmentPlayerFormatter extends LearnosityActivityFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'player' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $players = $this->entityTypeManager->getStorage('learnosity_activity_player')->loadMultiple();
    if (!empty($players)) {
      $options = [];
      foreach ($players as $player) {
        $options[$player->id()] = $player->label();
      }

      $form['player'] = [
        '#type' => 'select',
        '#title' => $this->t('Assessment player'),
        '#options' => $options,
        '#default_value' => $this->getSetting('player'),
        '#empty_option' => $this->t('Default'),
        '#description' => $this->t('Choose which player you want to use with this assessment.'),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $player = $this->t('Default');
    $entity = $this->entityTypeManager->getStorage('learnosity_activity_player')->load($this->getSetting('player'));
    if (!empty($entity)) {
      $player = $entity->label();
    }

    $summary[] = $this->t('Player: @player', [
      '@player' => $player,
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $host_entity = $items->getEntity();
    $user = $this->entityTypeManager->getStorage('user')->load(\Drupal::currentUser()->id());
    $mappingsHandler = \Drupal::service('learnosity.mappings_handler');

    foreach ($items as $delta => $item) {
      $view_value = $this->viewValue($item);
      $activity = $this->getActivityEntity($item);
      $data = $activity->getData(TRUE);

      // Fetch the learnosity activity player entity from the field settings.
      $entity_type = $this->entityTypeManager->getStorage('learnosity_activity_player')->load($this->getSetting('player'));
      if (!empty($entity_type)) {
        $config = $entity_type->getConfig(TRUE);
        $data['config'] = $config;
      }

      $elements[$delta] = [
        '#type' => 'learnosity_assessment_item',
        '#activity' => $activity,
        '#activity_template_id' => $view_value,
        '#activity_id' => $mappingsHandler->get($host_entity, 'activity_id'),
        '#name' => $mappingsHandler->get($host_entity, 'name'),
        '#user_id' => $mappingsHandler->get($user, 'id'),
        '#context' => [
          'entity' => $items->getEntity(),
          'activity_id' => $activity->id(),
          'user_id' => \Drupal::currentUser()->id(),
          'entity_type' => $items->getEntity()->getEntityTypeId(),
          'entity_id' => $items->getEntity()->id(),
          'langcode' => $langcode,
          'view_mode' => $this->viewMode,
        ],
      ];

      // If any configuration values are provided on the entity then pass
      // those in.
      if (isset($data['config'])) {
        $elements[$delta]['#config'] = $data['config'];
      }
      // Otherwise rely on the default element values.
      // Note that we grabbing those default values and re-applying them
      // because in this case we need to pass a redirect URL which needs to be
      // passed during runtime.
      else {
        $elementInfoManager = \Drupal::service('plugin.manager.element_info');
        $info = $elementInfoManager->getInfo('learnosity_assessment_item');
        $elements[$delta] = array_merge($info, $elements[$delta]);
      }

      // By default, always redirect back to this entity when submitted.
      // @todo: Make this configurable (Redirect vs AJAX).
      // See test:finished:submit in learnosity.init.js.
      if (!isset($elements[$delta]['#config']['configuration']['onsubmit_redirect_url'])) {
        $elements[$delta]['#config']['configuration']['onsubmit_redirect_url'] = $items->getEntity()->toUrl();
      }

    }

    return $elements;
  }

}
