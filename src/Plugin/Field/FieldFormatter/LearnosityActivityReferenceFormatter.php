<?php

namespace Drupal\learnosity\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "learnosity_activity_reference",
 *   label = @Translation("Reference ID"),
 *   description = @Translation("Display the referenced entities rendered by entity_view()."),
 *   field_types = {
 *     "learnosity_activity",
 *   }
 * )
 */
class LearnosityActivityReferenceFormatter extends LearnosityActivityFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#plain_text' => $this->viewValue($item)];
    }

    return $elements;
  }

}
