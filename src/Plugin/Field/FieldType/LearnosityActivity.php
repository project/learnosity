<?php

namespace Drupal\learnosity\Plugin\Field\FieldType;

use Drupal\Core\Field\PreconfiguredFieldUiOptionsInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\OptionsProviderInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Defines the 'entity_reference' entity field type.
 *
 * Supported settings (below the definition's 'settings' key) are:
 * - target_type: The entity type to reference. Required.
 *
 * @FieldType(
 *   id = "learnosity_activity",
 *   label = @Translation("Learnosity Activity"),
 *   description = @Translation("An entity field containing an entity reference."),
 *   category = @Translation("Reference"),
 *   default_widget = "learnosity_activity_authoring",
 *   default_formatter = "learnosity_assessment_player",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 *   cardinality = 1,
 * )
 */
class LearnosityActivity extends EntityReferenceItem implements OptionsProviderInterface, PreconfiguredFieldUiOptionsInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => \Drupal::moduleHandler()->moduleExists('learnosity') ? 'learnosity_activity' : '',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'editor' => [],
      'handler' => 'default',
      'handler_settings' => [],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $field = $form_state->getFormObject()->getEntity();

    $form = [
      '#type' => 'container',
      '#process' => [[get_class($this), 'fieldSettingsAjaxProcess']],
      '#element_validate' => [[get_class($this), 'fieldSettingsFormValidate']],
    ];

    $editors = \Drupal::entityTypeManager()->getStorage('learnosity_activity_editor')->loadMultiple();
    if (!empty($editors)) {
      $options = [];
      foreach ($editors as $editor) {
        $options[$editor->id()] = $editor->label();
      }

      $form['editor'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Allowed editors'),
        '#options' => $options,
        '#default_value' => $field->getSetting('editor'),
        '#description' => $this->t('Restrict which editors are allowed, given that the user has the required permissions. If no editors are selected, then the first on the user has access to will be available.'),
      ];
    }

    $form['handler'] = [
      '#type' => 'details',
      '#title' => $this->t('Handler settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#process' => [[get_class($this), 'formProcessMergeParent']],
    ];

    // Setting handler to hidden field to ensure it gets passed through.
    $form['handler']['handler'] = [
      '#type' => 'hidden',
      '#default_value' => $field->getSetting('handler'),
    ];

    $form['handler']['handler_settings'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['entity_reference-settings']],
    ];

    $handler = \Drupal::service('plugin.manager.entity_reference_selection')->getSelectionHandler($field);
    $form['handler']['handler_settings'] += $handler->buildConfigurationForm([], $form_state);

    return $form;
  }

}
