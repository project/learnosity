<?php

namespace Drupal\learnosity\Plugin\LearnosityFeature;

use Drupal\Core\Plugin\PluginBase;

/**
 * Class LeanrnosityFeatureBase.
 *
 * @package Drupal\learnosity\Plugin\LearnosityFeature
 */
abstract class LearnosityFeatureBase extends PluginBase implements LearnosityFeatureInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $definition = $this->getPluginDefinition();
    $feature = [];
    $feature['custom_type'] = $definition['id'];
    $feature['name'] = $definition['label'];
    $feature['js'] = (!empty($definition['js'])) ? $host . $definition['js'] : '';
    $feature['css'] = (!empty($definition['css'])) ? $host . $definition['css'] : '';
    $feature['version'] = $definition['version'];
    $feature['editor_layout'] = (!empty($definition['editor_layout'])) ? $host . $definition['editor_layout'] : '';
    $feature['editor_schema'] = $definition['editor_schema'];

    return $feature;
  }

  /**
   * {@inheritdoc}
   */
  public function buildAttachments() {
    $definition = $this->getPluginDefinition();
    if (!empty($definition['library'])) {
      return $definition['library'];
    }
    return [];
  }

}
