<?php

namespace Drupal\learnosity\Plugin\LearnosityFeature;

/**
 * Leanrosity Feature plugin interface.
 *
 * @package Drupal\learnosity\Plugin\LearnosityFeature
 */
interface LearnosityFeatureInterface {

  /**
   * Build the learnosity feature for the authoring experience.
   *
   * @return array
   *   Initialization array.
   */
  public function build();

  /**
   * Attach drupal library to to authoring experience.
   *
   * @return array
   *   Attachments array.
   */
  public function buildAttachments();

}
