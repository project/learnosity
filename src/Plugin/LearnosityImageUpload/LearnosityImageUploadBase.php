<?php

namespace Drupal\learnosity\Plugin\LearnosityImageUpload;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LeanrnosityFeatureBase.
 *
 * @package Drupal\learnosity\Plugin\LearnosityFeature
 */
abstract class LearnosityImageUploadBase extends PluginBase implements LearnosityImageUploadInterface {

  /**
   * Plugin settings array.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * Whether default settings have been merged into the current $settings.
   *
   * @var bool
   */
  protected $defaultSettingsMerged = FALSE;

  /**
   * The form default settings.
   *
   * @return array
   *   The default settings for this plugin.
   */
  public static function defaultSettings() {
    return [];
  }

  /**
   * Fetch the full settings array.
   *
   * @return array
   *   The full settings array.
   */
  public function getSettings() {
    // Merge defaults before returning the array.
    if (!$this->defaultSettingsMerged) {
      $this->mergeDefaults();
    }
    return $this->settings;
  }

  /**
   * Fetch a specific setting.
   *
   * @param string $key
   *   The setting key.
   *
   * @return mixed
   *   The value of the setting.
   */
  public function getSetting($key) {
    // Merge defaults if we have no value for the key.
    if (!$this->defaultSettingsMerged && !array_key_exists($key, $this->settings)) {
      $this->mergeDefaults();
    }
    return isset($this->settings[$key]) ? $this->settings[$key] : NULL;
  }

  /**
   * Merges default settings values into $settings.
   */
  protected function mergeDefaults() {
    $this->settings += static::defaultSettings();
    $this->defaultSettingsMerged = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setSettings(array $settings) {
    $this->settings = $settings;
    $this->defaultSettingsMerged = FALSE;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setSetting($key, $value) {
    $this->settings[$key] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm($form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(Request $request) {

  }

}
