<?php

namespace Drupal\learnosity\Plugin\LearnosityImageUpload;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface LearnosityFeatureInterface.
 *
 * @package Drupal\learnosity\Plugin\LearnosityFeature
 */
interface LearnosityImageUploadInterface {

  /**
   * Build the learnosity image upload settings form.
   *
   * @return array
   *   The form array.
   */
  public function settingsForm($form, FormStateInterface $form_state);

  /**
   * Get the current value.
   *
   * @return string
   *   The URL of the asset being uploaded.
   */
  public function getValue(Request $request);

}
