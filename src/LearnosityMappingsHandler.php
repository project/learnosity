<?php

namespace Drupal\learnosity;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class LearnosityMappingsHandler.
 *
 * This is a convenience class for handling the retrieval of mappings from
 * configuration.
 *
 * @package Drupal\learnosity
 */
class LearnosityMappingsHandler {

  /**
   * The config object.
   *
   * @var \Drupal\Core\Config\Config
   *   A configuration object.
   */
  protected $config;

  /**
   * Static cache of mappings.
   *
   * @var array
   */
  protected $mappings = [];

  /**
   * The required fields needed for it to function properly.
   *
   * @var array
   */
  protected $required = [
    'activity_id',
    'name',
  ];

  /**
   * LearnosityMappingsHandler constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->getEditable('learnosity.mappings');
  }

  /**
   * Get mappings from configuration.
   *
   * This is primarily for mapping user fields.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   *
   * @return array
   *   Array of field mappings.
   */
  protected function getFromConfig($entity_type, $bundle) {
    $mappings = $this->config->get($entity_type . '.' . $bundle);
    $this->mappings[$entity_type][$bundle] = $mappings;

    return $this->mappings[$entity_type][$bundle];
  }

  /**
   * Save mapping to configuration.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param array $mappings
   *   The mappings.
   */
  protected function saveConfig($entity_type, $bundle, array $mappings) {
    $this->config->set($entity_type . '.' . $bundle, $mappings);
    $this->config->save();
  }

  /**
   * Set a field mapping.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $key
   *   The mapping key.
   * @param string $field_name
   *   The field name.
   */
  public function setMapping($entity_type, $bundle, $key, $field_name) {
    $this->mappings[$entity_type][$bundle][$key] = $field_name;
  }

  /**
   * Set the mappings.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param array $mappings
   *   The mapping values.
   */
  public function setMappings($entity_type, $bundle, array $mappings) {
    $this->mappings[$entity_type][$bundle] = $mappings;
  }

  /**
   * Get a mapping.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $key
   *   The mapping key.
   */
  public function getMapping($entity_type, $bundle, $key) {
    $mappings = $this->getMappings($entity_type, $bundle);
    if (!empty($mappings[$key])) {
      return $mappings[$key];
    }
    return NULL;
  }

  /**
   * Get the mappings for an entity from third party settings.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return mixed
   *   The array of field mappings.
   */
  public function getMappings($entity_type, $bundle) {
    if (isset($this->mappings[$entity_type][$bundle])) {
      return $this->mappings[$entity_type][$bundle];
    }
    else {
      $mappings = $this->getFromConfig($entity_type, $bundle);
      $this->mappings[$entity_type][$bundle] = $mappings;
    }

    return $this->mappings[$entity_type][$bundle];
  }

  /**
   * Save the mapping values.
   */
  public function save() {
    foreach ($this->mappings as $entity_type => $bundles) {
      foreach ($bundles as $id => $mappings) {
        $this->saveConfig($entity_type, $id, $mappings);
      }
    }
  }

  /**
   * Fetches an entity's mapping information.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being mapped from.
   * @param string $key
   *   The mapping key.
   *
   * @return string|null
   *   The mapped value.
   */
  public function get(EntityInterface $entity, $key) {
    $mapping_config = $this->getMappings($entity->getEntityTypeId(), $entity->bundle());

    // Some keys are required in order to function properly.
    // Trigger an error if they are missing.
    if (in_array($key, $this->required) && !isset($mapping_config[$key])) {
      throw new \Exception("Error: Mapping not set for key: " . $key);
    }

    if (!empty($mapping_config[$key])) {
      $field_name = $mapping_config[$key];
      if ($entity->hasField($field_name)) {
        return $entity->get($field_name)->value;
      }
    }

    return NULL;
  }

}
