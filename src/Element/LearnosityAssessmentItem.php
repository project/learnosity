<?php

namespace Drupal\learnosity\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Render\Markup;

/**
 * Provides a Learnosity Assessment Player render element.
 *
 * Usage example:
 * @code
 * $build['assessment'] = [
 *   '#type' => 'learnosity_assessment_item',
 *   '#activity' => '',
 *   '#activity_template_id' => 'c73a51f5-6b53-4186-98a6-bcf1f800d552',
 *   '#activity_id' => 'basic-quiz',
 *   '#name' => 'Basic Quiz',
 *   '#user_id' => '',
 * ];
 * @endcode
 *
 * @RenderElement("learnosity_assessment_item")
 */
class LearnosityAssessmentItem extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#pre_render' => [
        [$class, 'preRenderLearnosityAssessmentPlayer'],
      ],
      '#config' => [],
      '#context' => [],
      '#theme' => 'learnosity_assessment_item',
    ];
  }

  /**
   * Pre render callback.
   *
   * The is originally based on the prerender callback defined on the link
   * element.
   *
   * @param array $element
   *   The element.
   *
   * @return array
   *   Element render array.
   */
  public static function preRenderLearnosityAssessmentPlayer(array $element) {

    $learnositySdk = \Drupal::service('learnosity.sdk');
    $learnositySession = \Drupal::service('learnosity.session_handler');
    $learnosityApiEventHandler = \Drupal::service('learnosity.api_event_handler');
    $config = \Drupal::config('learnosity.settings');

    // Generate or use the existing session.
    $session_id = $learnositySession->init($element['#activity_id']);

    // Attaching the service library to this render element. This will ensure
    // that the proper learnosity api library gets loaded.
    $element['#attached']['library'][] = 'learnosity/api.items';
    $element['#config'] = isset($element['#config']) ? $element['#config'] : [];

    $signed_request = $learnositySdk->init('items', [
      'activity_template_id' => $element['#activity_template_id'],
      'activity_id' => $element['#activity_id'],
      'name' => $element['#name'],
      'rendering_type' => 'assess',
      'type' => $config->get('disable_submit') ? 'local_practice' : 'submit_practice',
      'session_id' => $session_id,
      'user_id' => $element['#user_id'],
      'config' => $element['#config'],
    ], $element['#context']);

    $element['#attributes']['class'][] = 'learnosity-activity';

    // Make sure that session_id is always passed as part of the context
    // for assessment items. We can use this to determine most of the
    // contextual information. E.g user, activity etc.
    $element['#context'] += [
      'session_id' => $session_id,
    ];

    // Prepare the context items to be attached.
    // It will only attach scalar values because arrays and objects should not
    // be passed through javascript.
    $context = [];
    foreach ($element['#context'] as $key => $value) {
      if (is_scalar($value)) {
        $context[$key] = $value;
      }
    }

    // Load core learnosity drupal js file and pass signed request.
    $element['#attached']['library'][] = 'learnosity/learnosity';
    $element['#attached']['drupalSettings']['learnosity'] = [
      'service' => 'items',
      'signedRequest' => Markup::create($signed_request),
      'events' => $learnosityApiEventHandler->getSubscribedEvents(),
      'context' => $context,
    ];

    // Note: learnosity_assess id is required by learnosity in order for the
    // experience to load.
    $element['item'] = [
      '#markup' => Markup::create(' <div id="learnosity_assess" data-learnosity="true"></div>'),
    ];

    // We can't cache this because it needs an up-to-date session id.
    $element['#cache']['max-age'] = 0;

    return $element;
  }

}
