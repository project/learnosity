<?php

namespace Drupal\learnosity\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Render\Markup;

/**
 * Provides a Learnosity Reports render element.
 *
 * Usage example:
 * @code
 * $build['report'] = [
 *   '#type' => 'learnosity_reports_item',
 *   '#id' => '',
 *   '#reports' => [],
 * ];
 * @endcode
 *
 * @RenderElement("learnosity_reports_item")
 */
class LearnosityReportsItem extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#pre_render' => [
        [$class, 'preRenderLearnosityReportsItem'],
      ],
      '#reports' => [],
      '#context' => [],
      '#theme' => 'learnosity_reports_item',
    ];
  }

  /**
   * Pre render callback.
   *
   * The is originally based on the prerender callback defined on the link
   * element.
   *
   * @param array $element
   *   The element.
   *
   * @return array
   *   Element render array.
   */
  public static function preRenderLearnosityReportsItem(array $element) {

    $learnositySdk = \Drupal::service('learnosity.sdk');

    // Attaching the service library to this render element. This will ensure
    // that the proper learnosity api library gets loaded.
    $element['#attached']['library'][] = 'learnosity/api.reports';
    $element['#context'] = isset($element['#context']) ? $element['#context'] : [];

    $signed_request = $learnositySdk->init('reports', [
      'reports' => $element['#reports'],
    ], $element['#context']);

    // Prepare the context items to be attached.
    // It will only attach scalar values because arrays and objects should not
    // be passed through javascript.
    $context = [];
    foreach ($element['#context'] as $key => $value) {
      if (is_scalar($value)) {
        $context[$key] = $value;
      }
    }

    // It's necessary to add a suffix and prefix to accommodate
    // for loading via ajax.
    $element['#prefix'] = '<div class="learnosity-reports">';
    $element['#suffix'] = '</div>';
    $element['#attributes']['data-learnosity'] = 'true';
    // Load core learnosity drupal js file and pass signed request.
    $element['#attached']['library'][] = 'learnosity/learnosity';
    $element['#attached']['drupalSettings']['learnosity'] = [
      'service' => 'reports',
      'signedRequest' => Markup::create($signed_request),
      'context' => $context,
    ];

    // Create divs for each report item.
    $reports = [];
    foreach ($element['#reports'] as $report) {
      if (isset($report['id'])) {
        $reports[] = [
          '#markup' => '<div id="' . $report['id'] . '"></div>',
        ];
      }
    }
    $element['reports'] = $reports;
    return $element;
  }

}
