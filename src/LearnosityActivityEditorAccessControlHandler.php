<?php

namespace Drupal\learnosity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the learnosity editor entity type.
 *
 * @see \Drupal\learnosity\Entity\LearnosityActivityEditor
 */
class LearnosityActivityEditorAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $activity_editor, $operation, AccountInterface $account) {

    // All users are allowed to use the default editor.
    if ($operation == 'use') {
      return AccessResult::allowedIfHasPermission($account, $activity_editor->getPermissionName());
    }

    // The fallback editor may not be deleted.
    if ($operation == 'delete' && $activity_editor->isFallback()) {
      return AccessResult::forbidden();
    }

    if (in_array($operation, ['delete', 'update', 'view'])) {
      return parent::checkAccess($activity_editor, $operation, $account);
    }

    // No opinion.
    return AccessResult::neutral();
  }

}
