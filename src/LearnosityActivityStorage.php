<?php

namespace Drupal\learnosity;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\learnosity\Entity\LearnosityActivityInterface;

/**
 * Defines the storage handler class for Learnosity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Activity entities.
 *
 * @ingroup learnosity
 */
class LearnosityActivityStorage extends SqlContentEntityStorage implements LearnosityActivityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(LearnosityActivityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {learnosity_activity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {learnosity_activity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(LearnosityActivityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {learnosity_activity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('learnosity_activity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
