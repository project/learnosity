<?php

namespace Drupal\learnosity\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;

/**
 * Class LearnosityActivityEditorForm.
 *
 * The activity editor form.
 *
 * @package Drupal\learnosity\Form
 */
class LearnosityActivityEditorForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t("The human-readable name of this activity editor. This name must be unique."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\learnosity\Entity\LearnosityActivityEditor::load',
      ],
      '#disabled' => !$entity->isNew(),
    ];

    $form['settings'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Settings'),
    ];

    $learnosityImageUploadManager = \Drupal::service('plugin.manager.learnosity_image_upload');
    $definitions = $learnosityImageUploadManager->getDefinitions();
    $options = [];
    foreach ($definitions as $uploader) {
      $options[$uploader['id']] = $uploader['label'];
    }

    // If the form is submitted then rebuild the form with those values.
    $trigger = $form_state->getTriggeringElement();
    if (!empty($trigger['#name']) && $trigger['#name'] == 'upload_widget[plugin]') {
      $upload_widget = $form_state->getvalue('upload_widget');
    }
    else {
      $upload_widget = $entity->getUploadWidget();
    }

    $form['upload_widget'] = [
      '#type' => 'details',
      '#title' => $this->t('Image Upload'),
      '#open' => TRUE,
      '#group' => 'settings',
      '#tree' => TRUE,
    ];

    $form['upload_widget']['plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Image upload widget'),
      '#options' => $options,
      '#empty_option' => $this->t('Default'),
      '#default_value' => isset($upload_widget['plugin']) ? $upload_widget['plugin'] : '',
      '#description' => $this->t('Choose which widget you want to use for handling images.'),
      '#ajax' => [
        'callback' => '::uploadWidgetSettingsForm',
        'event' => 'change',
        'wrapper' => 'upload-widget-settings',
      ],
    ];
    // Disable caching on this form.
    $form_state->setCached(FALSE);

    $form['upload_widget']['settings'] = [
      '#type' => 'container',
      '#title' => $this->t('Settings'),
      '#open' => TRUE,
      '#attributes' => ['id' => 'upload-widget-settings'],
    ];

    $settings_form = [];
    // Pass the settings to the plugin form.
    if (!empty($upload_widget['plugin'])) {
      $plugin = $learnosityImageUploadManager->createInstance($upload_widget['plugin']);
      if (!empty($upload_widget['settings'])) {
        $plugin->setSettings($upload_widget['settings']);
      }
      $settings_form = $plugin->settingsForm([], $form_state);
    }

    foreach ($settings_form as $field => $form_item) {
      $form['upload_widget']['settings'][$field] = $form_item;
    }

    // Show custom features (if available).
    $learnosityFeatureManager = \Drupal::service('plugin.manager.learnosity_feature');
    $definitions = $learnosityFeatureManager->getDefinitions();

    if (!empty($definitions)) {
      $options = [];
      foreach ($definitions as $feature) {
        $options[$feature['id']] = $feature['label'];
      }
      $form['custom_features'] = [
        '#type' => 'details',
        '#title' => $this->t('Custom Features'),
        '#open' => FALSE,
        '#group' => 'settings',
      ];

      $form['custom_features']['features'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Enable Features'),
        '#options' => $options,
        '#default_value' => $entity->getFeatures(),
        '#description' => $this->t('Choose which custom features you want available from this editor.'),
      ];
    }

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced'),
      '#open' => FALSE,
      '#group' => 'settings',
    ];

    $form['advanced']['config'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Initialization Config'),
      '#default_value' => $entity->getConfig(),
      '#rows' => 20,
      '#description' => $this->t('The Learnosity Authoring API initialization settings. Leave blank to use Learnosity defaults. For more details see <a href = ":url">:url</a>.', [
        ':url' => 'https://reference.learnosity.com/author-api/initialization',
      ]),
    ];

    return $form;
  }

  /**
   * Upload Widget settings form callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form element.
   */
  public static function uploadWidgetSettingsForm(array &$form, FormStateInterface $form_state) {
    return $form['upload_widget']['settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    // Make sure that the Yaml is valid.
    $config = $form_state->getValue('config');
    try {
      Yaml::decode($config);
    }
    catch (\Exception $exception) {
      $form_state->setError($form['advanced']['config'], $exception->getMessage());
    }

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = $entity->save();

    switch ($status) {
      case SAVED_NEW:
        \Drupal::messenger()->addMessage($this->t('Created the %label Activity editor.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        \Drupal::messenger()->addMessage($this->t('Saved the %label Activity editor.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($entity->toUrl('collection'));
  }

}
