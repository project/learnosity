<?php

namespace Drupal\learnosity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Activity entities.
 *
 * @ingroup learnosity
 */
class LearnosityActivityDeleteForm extends ContentEntityDeleteForm {

}
