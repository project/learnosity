<?php

namespace Drupal\learnosity\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form to configure and rewrite settings.php.
 *
 * @internal
 */
class LearnositySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['learnosity.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'learnosity_settings_form';
  }

  /**
   * The allowed field types.
   *
   * Should be restricted to appropriate field lengths. For example, we don't
   * want to allow users to map to textarea or boolean fields.
   *
   * @return array
   *   Array of allowed field types.
   */
  protected function allowedFieldTypes() {
    return [
      'string',
      'integer',
      'uuid',
      'email',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $mappingsHandler = \Drupal::service('learnosity.mappings_handler');

    $config = $this->config('learnosity.settings');
    $form['auth_keys'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Consumer authentication keys'),
      '#description' => $this->t('The consumer authentication key and secret can be generated from the Learnosity Console.'),
    ];
    $form['auth_keys']['consumer_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Consumer Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('consumer_key'),
    ];
    // Only make consumer secret required if one hasn't been provided.
    $form['auth_keys']['consumer_secret'] = [
      '#type' => 'password',
      '#required' => (empty($config->get('consumer_secret'))),
      '#title' => $this->t('Consumer Secret'),
      '#default_value' => $config->get('consumer_secret'),
    ];
    $form['mappings'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];
    $form['mappings']['user'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('User Field Mapping'),
    ];

    $user_fields = [];
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $field_definitions = $entityFieldManager->getBaseFieldDefinitions('user');
    foreach ($field_definitions as $field) {
      if (in_array($field->getType(), $this->allowedFieldTypes())) {
        $user_fields[$field->getName()] = $field->getLabel();
      }
    }

    $form['mappings']['user']['id'] = [
      '#type' => 'select',
      '#title' => $this->t('User ID'),
      '#options' => $user_fields,
      '#description' => $this->t('The ID of the user who is attempting or authoring the assessment. Limit 50 characters. User UUID recommended.'),
      '#required' => TRUE,
      '#default_value' => $mappingsHandler->getMapping('user', 'user', 'id'),
    ];

    $form['mappings']['user']['firstname'] = [
      '#type' => 'select',
      '#title' => $this->t('First name'),
      '#options' => $user_fields,
      '#description' => $this->t('The first name of the user who is authoring the assessment.'),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $mappingsHandler->getMapping('user', 'user', 'firstname'),
    ];

    $form['mappings']['user']['lastname'] = [
      '#type' => 'select',
      '#title' => $this->t('Last name'),
      '#options' => $user_fields,
      '#description' => $this->t('The last name of the user who is authoring the assessment.'),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $mappingsHandler->getMapping('user', 'user', 'lastname'),
    ];
    $form['mappings']['user']['email'] = [
      '#type' => 'select',
      '#title' => $this->t('Email'),
      '#options' => $user_fields,
      '#description' => $this->t('The email of the user who is authoring the assessment.'),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $mappingsHandler->getMapping('user', 'user', 'email'),
    ];
    $form['debug'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Debugging'),
    ];
    $form['debug']['disable_submit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable submissions'),
      '#description' => $this->t('Users can interact with assessments without sending data to Learnosity.'),
      '#default_value' => $config->get('disable_submit'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('learnosity.settings')
      ->set('consumer_key', $form_state->getValue('consumer_key'))
      ->set('disable_submit', $form_state->getValue('disable_submit'))
      ->save();

    // Only save the secret if one is provided. this way we don't need to
    // provide it every time we make a change to settings.
    if (!empty($form_state->getValue('consumer_secret'))) {
      $this->config('learnosity.settings')
        ->set('consumer_secret', $form_state->getValue('consumer_secret'))
        ->save();
    }

    // Save the user field mappings.
    $mappingsHandler = \Drupal::service('learnosity.mappings_handler');
    $mappings = $form_state->getValue(['mappings', 'user']);
    $mappingsHandler->setMappings('user', 'user', $mappings);
    $mappingsHandler->save();


    parent::submitForm($form, $form_state);

  }

}
