<?php

namespace Drupal\learnosity\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a LearnosityFeature object.
 *
 * @Annotation
 */
class LearnosityFeature extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the editor plugin.
   *
   * @var string
   */
  public $label;

  /**
   * The unique uuid for this feature. (Used with simple features).
   *
   * @var string
   */
  public $uuid;

  /**
   * The path to the javascript file.
   *
   * @var string
   */
  public $js;

  /**
   * The path to the css file.
   *
   * @var string
   */
  public $css;

  /**
   * The path to the html layout.
   *
   * @var string
   */
  public $editor_layout;

  /**
   * The current version of this feature.
   *
   * @var string
   */
  public $version;

  /**
   * The editor schema.
   *
   * @var array
   */
  public $editor_schema = [];

}
