<?php

namespace Drupal\learnosity\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a LearnosityImageUpload object.
 *
 * @Annotation
 */
class LearnosityImageUpload extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the editor plugin.
   *
   * @var string
   */
  public $label;

  /**
   * The entity type.
   *
   * @var string
   */
  public $entity_type;

  /**
   * The bundle of the entity.
   *
   * @var string
   */
  public $bundle;

  /**
   * The modal settings.
   *
   * @var array
   */
  public $modal_settings = [];

}
