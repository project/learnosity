<?php

namespace Drupal\learnosity\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Ajax command for polling learnosity.
 *
 * This command instructs learnosity.init.js to poll learnosity during an event.
 * This helps in cases where data might not yet be available, so we need to
 * perform another ajax request until it is. If this is not returned then it
 * will only do one ajax request.
 *
 * @ingroup ajax
 */
class PollLearnosityCommand implements CommandInterface {

  /**
   * {@inheritdoc}
   */
  public function render() {

    return [
      'command' => 'pollLearnosity',
    ];
  }

}
