<?php

namespace Drupal\learnosity\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * AJAX command to update the selected value.
 */
class LearnosityImageUpdateCommand implements CommandInterface {

  /**
   * The target field to update.
   *
   * @var string
   */
  protected $target;

  /**
   * The values to update.
   *
   * @var string
   */
  protected $value;

  /**
   * LearnosityImageUpdateCommand constructor.
   *
   * @param string $target
   *   The target.
   * @param string $value
   *   The value of the file.
   */
  public function __construct($target, $value) {
    $this->target = $target;
    $this->value = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'learnosity_image_update',
      'target' => $this->target,
      'value' => $this->value,
    ];
  }

}
