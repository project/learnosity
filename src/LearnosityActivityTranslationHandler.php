<?php

namespace Drupal\learnosity;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for learnosity activities.
 */
class LearnosityActivityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
