<?php

namespace Drupal\learnosity;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * The Learnosity Feature plugin manager.
 *
 * @package Drupal\learnosity\Plugin
 */
class LearnosityFeaturePluginManager extends DefaultPluginManager {

  /**
   * The Learnosity Feature plugin manager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/LearnosityFeature', $namespaces, $module_handler, 'Drupal\learnosity\Plugin\LearnosityFeature\LearnosityFeatureInterface', 'Drupal\learnosity\Annotation\LearnosityFeature');

    $this->alterInfo('learnosity_feature');
    $this->setCacheBackend($cache_backend, 'learnosity_feature');
  }

}
