<?php

namespace Drupal\learnosity\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\OpenDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormState;
use Drupal\learnosity\Ajax\LearnosityImageUpdateCommand;
use Drupal\learnosity\LearnosityImageUploadPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Builds the image upload modal.
 */
class LearnosityImageUploadController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The image upload plugin manager.
   *
   * @var \Drupal\learnosity\LearnosityImageUploadPluginManager
   */
  protected $imageUploadManager;

  /**
   * Constructs a LearnosityImageUploadController object.
   *
   * @param \Drupal\learnosity\LearnosityImageUploadPluginManager $image_upload_manager
   *   The Learnosity Image Upload Plugin Manager.
   */
  public function __construct(LearnosityImageUploadPluginManager $image_upload_manager) {
    $this->imageUploadManager = $image_upload_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.learnosity_image_upload')
    );
  }

  /**
   * Open image upload dialog box.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $editor
   *   The activity editor.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function uploadForm(Request $request, $editor) {
    // Load the creation form of a given entity type. The entity type is
    // defined as part of the annotation of the LearnosityImageUpload plugin.
    $editor = $this->entityTypeManager()->getStorage('learnosity_activity_editor')->load($editor);

    $upload_widget = $editor->getUploadWidget();
    $plugin = $this->imageUploadManager->createInstance($upload_widget['plugin']);
    $plugin->setSettings($upload_widget['settings']);
    $definition = $plugin->getPluginDefinition();

    // @todo: Get this working with regular forms as well.
    $entity_definition = $this->entityTypeManager()->getDefinition($definition['entity_type']);
    $bundleKey = $entity_definition->getKey('bundle');
    $entity = $this->entityTypeManager()->getStorage($definition['entity_type'])->create([
      $bundleKey => $definition['bundle'],
    ]);
    $form_object = \Drupal::service('class_resolver')->getInstanceFromDefinition($entity_definition->getFormClass('default'));
    $form_state = (new FormState())
      ->setFormObject($form_object)
      ->disableRedirect();

    // Fetch the full URL and update the content.
    $value = $plugin->getValue($request);
    if (empty($value) && $form_state && !$form_state->isExecuted()) {
      $form = \Drupal::service('entity.form_builder')->getForm($entity);
      $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
      $form['#attached']['library'][] = 'learnosity/image_upload';
      $form['actions']['submit']['#attributes']['class'][] = 'use-ajax-submit';
      // We're storing the selected value in a hidden field at the bottom of
      // the page. As a result, after AJAX submit it refocuses on that hidden
      // field. Disabling refocus.
      $form['actions']['submit']['#attributes']['data-disable-refocus'] = 'true';

      $response = new AjaxResponse();
      // @todo: Populate these values from the plugin annotation.
      $response->addCommand(new OpenDialogCommand('#image-dialog', 'Upload image', $form, [
        'width' => '80%',
        'height' => 'auto',
        'modal' => TRUE,
        'maxWidth' => 800,
        'maxHeight' => 800,
        'fluid' => 1,
        'autoResize' => 0,
        'resizable' => 0,
      ]));

      return $response;
    }
    // Once selected, embed the image.
    else {
      $target = $request->get('target');
      $response = new AjaxResponse();
      $response->addCommand(new CloseDialogCommand('#image-dialog'));
      $response->addCommand(new LearnosityImageUpdateCommand($target, $value));

      return $response;
    }
  }

}
