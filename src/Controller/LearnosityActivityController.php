<?php

namespace Drupal\learnosity\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\learnosity\Entity\LearnosityActivityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;

/**
 * Class LearnosityActivityController.
 *
 * Returns responses for Learnosity Activity routes.
 */
class LearnosityActivityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a LearnosityActivityController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   */
  public function __construct(DateFormatter $date_formatter, Renderer $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays an Activity revision.
   *
   * @param int $learnosity_activity_revision
   *   The Activity revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($learnosity_activity_revision) {
    $entity = $this->entityTypeManager()->getStorage('learnosity_activity')->loadRevision($learnosity_activity_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('learnosity_activity');

    return $view_builder->view($entity);
  }

  /**
   * Page title callback for a Learnosity Activity revision.
   *
   * @param int $learnosity_activity_revision
   *   The Learnosity activity revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($learnosity_activity_revision) {
    $entity = $this->entityTypeManager()->getStorage('learnosity_activity')->loadRevision($learnosity_activity_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $entity->label(),
      '%date' => $this->dateFormatter->format($entity->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of an activity.
   *
   * @param \Drupal\learnosity\Entity\LearnosityActivityInterface $entity
   *   A Learnosity Activity object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(LearnosityActivityInterface $entity) {
    $account = $this->currentUser();
    $langcode = $entity->language()->getId();
    $langname = $entity->language()->getName();
    $languages = $entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $storage = $this->entityTypeManager()->getStorage('learnosity_activity');

    if ($has_translations) {
      $build['#title'] = $this->t('@langname revisions for %title', [
        '@langname' => $langname,
        '%title' => $entity->label(),
      ]);
    }
    else {
      $build['#title'] = $this->t('Revisions for %title', ['%title' => $entity->label()]);
    }

    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all learnosity activity revisions") || $account->hasPermission('administer learnosity activity entities')));
    $delete_permission = (($account->hasPermission("delete all learnosity activity revisions") || $account->hasPermission('administer learnosity activity entities')));

    $rows = [];

    $vids = $storage->revisionIds($entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\learnosity\Interface $revision */
      $revision = $storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.learnosity_activity.revision', [
            'learnosity_activity' => $entity->id(),
            'learnosity_activity_revision' => $vid,
          ]));
        }
        else {
          $link = $entity->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            if ($has_translations) {
              $url = Url::fromRoute('entity.learnosity_activity.translation_revert', [
                'learnosity_activity' => $entity->id(),
                'learnosity_activity_revision' => $vid,
                'langcode' => $langcode,
              ]);
            }
            else {
              $url = Url::fromRoute('entity.learnosity_activity.revision_revert', [
                'learnosity_activity' => $entity->id(),
                'learnosity_activity_revision' => $vid,
              ]);
            }
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $url,
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.learnosity_activity.revision_delete', [
                'learnosity_activity' => $entity->id(),
                'learnosity_activity_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['learnosity_activity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
