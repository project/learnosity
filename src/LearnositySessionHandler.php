<?php

namespace Drupal\learnosity;

use Drupal\Core\Database\Connection;

/**
 * Class LearnositySessionHandler.
 *
 * This is a lightweight class that manages the current active learnosity
 * sessions. This is created so that we have some control on how sessions are
 * created on assessments.
 *
 * @package Drupal\learnosity
 */
class LearnositySessionHandler {

  /**
   * The learnosity Sdk service.
   *
   * @var \Drupal\learnosity\LearnositySdk
   */
  protected $learnosity;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * LearnositySessionHandler constructor.
   *
   * @param \Drupal\learnosity\LearnositySdk $learnosity
   *   The learnosity sdk service.
   * @param \Drupal\Core\Database\Connection $database
   *   The drupal core database.
   */
  public function __construct(LearnositySdk $learnosity, Connection $database) {
    $this->learnosity = $learnosity;
    $this->database = $database;
  }

  /**
   * Fetch the user's Learnosity session ID.
   *
   * @param int $activity_id
   *   The activity id associated with this session.
   *
   * @return string
   *   The user's Learnosity session id.
   */
  public function getUserSessionId($activity_id) {
    $session = $this->database->select('learnosity_sessions', 'ls')
      ->fields('ls', ['sid'])
      ->condition('uid', \Drupal::currentUser()->id())
      ->condition('activity_id', $activity_id)
      ->condition('status', 0)
      ->execute()
      ->fetchCol();

    if (!empty($session[0])) {
      return $session[0];
    }

    return FALSE;
  }

  /**
   * Initialize the session.
   *
   * @param int $activity_id
   *   The activity id.
   *
   * @return string
   *   The session id.
   *
   * @throws \Exception
   */
  public function init($activity_id) {
    // Don't initialize a new session if one already exists.
    $sid = $this->getUserSessionId($activity_id);
    if ($sid) {
      return $sid;
    }
    $sid = $this->generateSessionUuid();
    $this->database->insert('learnosity_sessions')
      ->fields([
        'sid' => $sid,
        'uid' => \Drupal::currentUser()->id(),
        'activity_id' => $activity_id,
        'timestamp' => \Drupal::time()->getRequestTime(),
      ])->execute();

    return $sid;
  }

  /**
   * Expires a specific session.
   *
   * @param string $sid
   *   The session id.
   */
  public function expire($sid) {
    $this->database->update('learnosity_sessions')
      ->fields([
        'status' => 1,
      ])
      ->condition('sid', $sid)
      ->execute();
  }

  /**
   * Prune old sessions from the database.
   */
  public function prune() {

    // Remove sessions that have expired or are more than 12 hours old.
    $query = $this->database->delete('learnosity_sessions');
    $or_group = $query->orConditionGroup()
      ->condition('status', 1)
      ->condition('timestamp', \Drupal::time()->getRequestTime() - (3600 * 12), '<');

    $query->condition($or_group)
      ->execute();
  }

  /**
   * Generates the Session uuid.
   *
   * @return string
   *   The session uuid.
   */
  public function generateSessionUuid() {
    return $this->learnosity->generateUuid();
  }

}
