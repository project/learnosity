<?php

namespace Drupal\learnosity;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * The Learnosity Image Upload plugin manager.
 *
 * @package Drupal\learnosity
 */
class LearnosityImageUploadPluginManager extends DefaultPluginManager {

  /**
   * The Learnosity Image Upload plugin manager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/LearnosityImageUpload', $namespaces, $module_handler, 'Drupal\learnosity\Plugin\LearnosityImageUpload\LearnosityImageUploadInterface', 'Drupal\learnosity\Annotation\LearnosityImageUpload');

    $this->alterInfo('learnosity_image_upload');
    $this->setCacheBackend($cache_backend, 'learnosity_image_upload');
  }

}
