<?php

namespace Drupal\learnosity\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\learnosity\LearnositySdk;
use Drush\Commands\DrushCommands;

/**
 * Learnosity Drush commands for Drush 9.x.
 *
 * Class LearnosityCommands.
 *
 * @package Drupal\learnosity\Commands
 */
class LearnosityCommands extends DrushCommands {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The learnosity Sdk service.
   *
   * @var \Drupal\learnosity\LearnositySdk
   */
  protected $learnosity;

  /**
   * LearnosityCommands constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\learnosity\LearnositySdk $learnosity
   *   The learnosity sdk service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LearnositySdk $learnosity) {
    parent::__construct();
    $this->entityTypeManager = $entity_type_manager;
    $this->learnosity = $learnosity;
  }

  /**
   * Compare activities between Learnosity and Drupal.
   *
   * @param array $options
   *   Additional options for the command.
   *
   * @command learnosity:audit-activities
   *
   * @usage learnosity:audit-activites
   *   Show a table of activities and their entity ids.
   * @option entity-id Filter by entity id or TRUE/FALSE
   * @usage learnosity:audit-activites --format=json
   * @table-style default
   * @field-labels
   *   reference: Reference
   *   title: Title
   *   items: # Items
   *   status: Status
   *   entity-id: Entity ID
   * @default-fields reference,title,items,status,entity-id
   * @aliases laudit
   * @filter-default-field reference
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   Activity details formatted as table.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function auditActivities(array $options = ['entity-id' => self::REQ, 'format' => 'table']) {
    $rows = [];
    // Doing a recursive call. By default, it only returns 50 activities at
    // a time. This ensures that we get all activities.
    $results = $this->learnosity->dataApi('itembank/activities', [], NULL, TRUE);

    if (isset($options['entity-id'])) {
      // Boolean values get passed in as strings.
      if (!is_numeric($options['entity-id'])) {
        $options['entity-id'] = ($options['entity-id'] == 'false') ? FALSE : TRUE;
      }
    }

    foreach ($results as $item) {
      $row = [
        'reference' => $item['reference'],
        'title' => $item['title'],
        'items' => (isset($item['data']['items'])) ? count($item['data']['items']) : 0,
        'status' => $item['status'],
        'entity-id' => NULL,
      ];

      $activity = $this->entityTypeManager->getStorage('learnosity_activity')->getQuery()
        ->accessCheck(TRUE)
        ->condition('reference', $item['reference'])
        ->execute();

      if (!empty($activity)) {
        $row['entity-id'] = array_pop($activity);
      }

      if (isset($options['entity-id'])) {
        if (is_numeric($options['entity-id'])) {
          if ($options['entity-id'] != $row['entity-id']) {
            continue;
          }
        }
        else {
          if ($options['entity-id'] != $row['entity-id']) {
            continue;
          }
        }
      }

      $rows[] = $row;
    }

    if (!empty($rows)) {
      return new RowsOfFields($rows);
    }
  }

}
