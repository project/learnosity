<?php

namespace Drupal\learnosity;

use Drupal\learnosity\Event\LearnosityAssessmentEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Learnosity API Event handler.
 *
 * @package Drupal\learnosity
 */
class LearnosityApiEventHandler {

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Array of registered events.
   *
   * @var array
   */
  protected $subscribedEvents = [];

  /**
   * LearnosityApiEventHandler constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   */
  public function __construct(EventDispatcherInterface $event_dispatcher) {
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Get all learnosity events.
   *
   * @return array
   *   The learnosity events.
   */
  public function getLearnosityEvents() {
    return LearnosityAssessmentEvent::getEvents();
  }

  /**
   * Fetches all subscribed learnosity events.
   *
   * @return array
   *   An array of learnosity events with prefix removed (used for
   *   javascript events).
   */
  public function getSubscribedEvents() {
    // @todo: Add caching.
    // Learnosity needs to know which events it should pass to its 'on'
    // listener. We only want to pass events that we're actually subscribed to.
    // See learnosity.js (drupalSettings.learnosity.events).
    if (empty($this->subscribedEvents)) {
      $events = $this->getLearnosityEvents();
      foreach ($events as $name) {
        if ($this->eventDispatcher->hasListeners($name)) {
          $this->subscribedEvents[] = $this->parseEventName($name);
        }
      }
    }
    return $this->subscribedEvents;
  }

  /**
   * Parses the name to work with learnosity javascript 'on' events.
   *
   * @param string $name
   *   The event name.
   *
   * @return string
   *   The proper learnosity api event name.
   */
  public function parseEventName($name) {
    return str_replace('learnosity.', '', $name);
  }

}
