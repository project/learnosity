<?php

namespace Drupal\learnosity\Event;

use Drupal\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Response;

/**
 * Represents an event triggered by learnosity.
 */
class LearnosityEvent extends Event {

  /**
   * The name of the event.
   *
   * @var string
   */
  protected $name;

  /**
   * The context.
   *
   * @var array
   */
  protected $context;

  /**
   * The return response object.
   *
   * @var \Symfony\Component\HttpFoundation\Response
   */
  protected $response;

  /**
   * Creates the LearnosityEvent object.
   *
   * @param string $name
   *   The name of the event.
   * @param array $context
   *   The context array.
   */
  public function __construct($name, array $context) {
    $this->name = $name;
    $this->context = $context;
  }

  /**
   * Fetches the event name.
   *
   * @return string
   *   The learnosity event name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Fetches the context.
   *
   * @return array
   *   The context array.
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * Sets the return response object.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The return response.
   */
  public function setResponse(Response $response) {
    $this->response = $response;
  }

  /**
   * Gets the return response object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The return response.
   */
  public function getResponse() {
    return $this->response;
  }

}
