<?php

namespace Drupal\learnosity\Event;

/**
 * Class LearnosityAssessmentEvent.
 *
 * Learnosity api assessment events to constants for event subscriber support.
 *
 * See: https://reference.learnosity.com/items-api/events#assessmentEvents.
 *
 * @package Drupal\learnosity\Event
 */
final class LearnosityAssessmentEvent {

  const DYNAMIC_ITEM_CHANGED = 'learnosity.dynamic:item:changed';

  const FOCUSED = 'learnosity.focused';

  const ITEM_CHANGED = 'learnosity.item:changed';

  const ITEM_CHANGING = 'learnosity.item:changing';

  const ITEMS_LAZYLOAD_BATCH = 'learnosity.items:lazyload:batch';

  const ITEMS_LAZYLOAD_COMPLETE = 'learnosity.items:lazyload:complete';

  const ITEMS_LAZYLOAD_ERROR = 'learnosity.items:lazyload:error';

  const ITEM_LOAD = 'learnosity.item:load';

  const ITEM_SETATTEMPTEDRESPONSE = 'learnosity.item:setAttemptedResponse';

  const ITEM_BEFOREUNLOAD = 'learnosity.item:beforeunload';

  const ITEM_UNLOAD = 'learnosity.item:unload';

  const ITEM_WARNINGONCHANGE = 'learnosity.item:warningOnChange';

  const ITEMS_FETCH = 'learnosity.items:fetch';

  const ITEMS_FETCH_DONE = 'learnosity.items:fetch:done';

  const SECTION_CHANGED = 'learnosity.section:changed';

  const FINISHED_DISCARD = 'learnosity.test:finished:discard';

  const FINISHED_SAVE = 'learnosity.test:finished:saved';

  const FINISHED_SUBMIT = 'learnosity.test:finished:submit';

  const PANEL_HIDE = 'learnosity.test:panel:hide';

  const PANEL_SHOW = 'learnosity.test:panel:show';

  const PAUSE = 'learnosity.test:pause';

  const READING_END = 'learnosity.reading:end';

  const READING_START = 'learnosity.reading:start';

  const READY = 'learnosity.test:ready';

  const RESUME = 'learnosity.test:resume';

  const SAVE = 'learnosity.test:save';

  const SAVE_ERROR = 'learnosity.test:save:error';

  const SAVE_SUCCESS = 'learnosity.test:save:success';

  const START = 'learnosity.test:start';

  const SUBMIT = 'learnosity.test:submit';

  const SUBMIT_ERROR = 'learnosity.test:submit:error';

  const SUBMIT_SUCCESS = 'learnosity.test:submit:success';

  const TIME_CHANGE = 'learnosity.time:change';

  const TIME_END = 'learnosity.time:end';

  const PROCTOR_EXITANDSUBMIT = 'learnosity.proctor:exitAndSubmit';

  const PROCTOR_GOTO = 'learnosity.proctor:goto';

  const PROCTOR_PAUSED = 'learnosity.proctor:paused';

  const PROCTOR_RESUMED = 'learnosity.proctor:resumed';

  const PROCTOR_SAVE = 'learnosity.proctor:save';

  const PROCTOR_TERMINATED = 'learnosity.proctor:terminated';

  const UNFOCUSED = 'learnosity.unfocused';

  const SAVE_ACTIVITY_SUCCESS = 'learnosity.save:activity:success';

  /**
   * Return all the constants in an array.
   *
   * @return array
   *   Array of constants.
   */
  public static function getEvents() {
    $reflection = new \ReflectionClass(LearnosityAssessmentEvent::class);
    return $reflection->getConstants();
  }

}
