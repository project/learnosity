(function ($, window, Drupal, drupalSettings) {
  Drupal.learnosityHandlers.addHandler('authorapi', {

    // Persist uploaded values in order to prevent appending images multiple
    // times.
    hiddenValue: '',

    // The hidden element id.
    hiddenId: 'learnosity-asset',

    updateActivityFields: function (app) {
      // If a new activity was chosen make sure that the reference ID is
      // updated so that we associate the the right activity with the
      // entity.
      app.on('open:activity', function(event) {
        if (typeof(event['data'].reference) !== 'undefined') {
          $('#lrn-field--reference').val(event['data'].reference);
        }
      });

      // If an activity is saved then export the data and save it in the data field.
      // this makes it easier for us to initialize.
      var activityEvents = [
        'activityedit:saveable:changed',
      ];
      for (var event in activityEvents) {
        app.on(activityEvents[event], function(e) {
          var activity = app.getActivity();
          var json = encodeURIComponent(JSON.stringify(activity.data));
          $('#lrn-field--data').val(json);
          // Also make sure that the reference is always valid.
          // This especially helps address an issue with duplication.
          if ($('#lrn-field--reference').val() !== activity.reference) {
            $('#lrn-field--reference').val(activity.reference);
          }
        });
      }
    },

    libReady: function () {
      return (typeof LearnosityAuthor != 'undefined');
    },

    init: function (initObj, callbacks) {
      var _this = this;

      // Override asset request with custom upload if configured.
      if (drupalSettings.learnosity.uploadWidget) {
        callbacks.assetRequest = function (mediaRequested, returnType, callback, attributes) {
          _this.assetRequest(mediaRequested, returnType, callback, attributes, _this.app);
        };
      }
      return LearnosityAuthor.init(initObj, callbacks);
    },

    onAppReady: function (app) {
      var _this = this;

      _this.updateActivityFields(app);

      // Learnosity introduced a bug in v2023.2.LTS which would cause the
      // page to reload when selecting an item while authoring an activity.
      // type = "button" attribute prevents the submit behavior.
      app.on('activityedit:item:edit', function () {
        $('button.lrn-h3').attr('type', 'button');
      });

      // Reset the hidden asset fields on load if they exist.
      app.on('widgetedit:editor:ready', function (data) {
        $('.lrn-asset').each(function() {
          this.remove();
        });
      });

      // When the node form is submitted, save it and continue submitting
      // the form.
      // TODO: Submit handler works however, could we make it more robust?
      $('form').submit(function (event) {
        // Only save the activity if there was a change.
        app.saveActivity();
        return true;
      });
    },

    // https://reference.learnosity.com/author-api/initialization#assetRequest
    assetRequest: function(mediaRequested, returnType, callback, attributes, app) {
      var _this = this;

      // When assetRequest is invoked it always expects a callback event
      // when cancelled. Reset the callback so that it can continue to be
      // used.
      $(window).on('dialog:afterclose', function (element) {
        callback();
      });

      // Append a hidden field to the body. This is used to temporarily store
      // the new image.
      var random = Math.random().toString().substr(2, 7);
      var assetId = 'lrn-asset-' + random;
      var $hidden = $('<input>', {type: "hidden", class:'lrn-asset', id: assetId});
      $('body').append($hidden);

      $hidden.on('change', function () {
        let src = $(this).attr('value');
        // Only execute this if the value has changed.
        if (_this.hiddenValue !== src) {
          _this.hiddenValue = src;

          switch (returnType) {
            case 'HTML':
              // TODO: Alt tag.
              var $img = '<img src = "' + src + '">';
              callback($img);
              break;

            case 'URL':
            case 'UrlHeightWidth':
              callback(src);
              break;
          }
        }
      });

      // Open the image browser.
      var editor = drupalSettings.learnosity.editor;
      var ajaxObj = Drupal.ajax({
        url: '/learnosity/image-upload/' + editor + '?target=' + assetId,
        element: false,
        progress: {}
      });
      ajaxObj.success = function (response, status) {
        // Execute the AJAX commands.
        Object.keys(response || {}).forEach(i => {
          if (response[i].command && this.commands[response[i].command]) {
            this.commands[response[i].command](this, response[i], status);
          }
        });
      };
      ajaxObj.execute();

    },

  });

})(jQuery, window, Drupal, drupalSettings);
