(function ($, window, Drupal, drupalSettings) {
  Drupal.learnosityHandlers.addHandler('items', {
    /** This prevents the "leave site" prompt for inline items. */
    beforeunload: false,
  });
})(jQuery, window, Drupal, drupalSettings);
