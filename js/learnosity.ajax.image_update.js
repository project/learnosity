/**
 * @file learnosity.ajax.image_update.js
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Command to update the image upload value.
   *
   * @param {Drupal.Ajax} [ajax]
   *   The ajax object.
   * @param {object} response
   *   Object holding the server response.
   * @param {string} response.value
   *   The value of the element.
   * @param {number} [status]
   *   The HTTP status code.
   */
  Drupal.AjaxCommands.prototype.learnosity_image_update = function (ajax, response, status) {
    // Change the value and then update the form.
    var element = $('#' + response.target);
    element.val(response.value).trigger('change');
  };

}(jQuery, Drupal, drupalSettings));
