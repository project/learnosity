<?php

namespace Drupal\Tests\learnosity\Kernel;

use Drupal\learnosity\Entity\LearnosityActivity;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\user\Entity\User;

/**
 * @coversDefaultClass \Drupal\learnosity\Plugin\Field\FieldFormatter\LearnosityAssessmentPlayerFormatter
 * @group learnosity
 */
class LearnosityAssessmentPlayerFormatterTest extends LearnosityKernelTestBase {

  /**
   * Disable schema validation when running tests.
   *
   * @var bool
   *
   * @todo: Fix this by providing actual schema validation.
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'user',
    'system',
    'field',
    'learnosity',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create article content type.
    $values = ['type' => 'article', 'name' => 'Article'];
    $node_type = NodeType::create($values);
    $node_type->save();

    // Add the learnosity_activity field to article.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_learnosity',
      'entity_type' => 'node',
      'type' => 'learnosity_activity',
      'settings' => [
        'target_type' => 'learnosity_activity'
      ],
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'article',
      'settings' => [
        'editor' => [
          'default' => 'default',
        ]
      ]
    ]);
    $field->save();

    $this->testUser = User::create([
      'name' => 'foobar1',
      'mail' => 'foobar1@example.com',
    ]);
    $this->testUser->save();
    \Drupal::currentUser()->setAccount($this->testUser);
  }

  /**
   * Test the Assessment Player field formatter.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testAssessmentPlayerFormatter() {
    // Create the test LearnosityActivity entity.
    $uuid = '12345';
    $activity = LearnosityActivity::create([
      'reference' => $uuid,
    ]);
    $activity->save();

    $node = Node::create([
      'title' => $this->randomString(16),
      'type' => 'article',
      'field_learnosity' => $activity,
    ]);
    $node->save();

    // @todo: Look at FieldDisplayTest.
    $result = $node->field_learnosity->view(['type' => 'learnosity_assessment_player']);
    $this->setRawContent($this->render($result));
    $this->assertRaw("<div id=\"learnosity_assess\" data-learnosity=\"true\"></div>");
  }

}
