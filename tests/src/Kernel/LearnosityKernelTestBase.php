<?php

namespace Drupal\Tests\learnosity\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Base class for Learnosity kernel tests.
 */
abstract class LearnosityKernelTestBase extends KernelTestBase {

  use UserCreationTrait;

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'user',
    'node',
    'system',
    'learnosity',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('learnosity_activity');
    $this->installSchema('system', ['sequences']);
    $this->installSchema('learnosity', ['learnosity_sessions']);
    $this->installConfig('system');
    $this->installConfig('learnosity');

    // Set connection information.
    $config_factory = \Drupal::service('config.factory');
    $config_factory->getEditable('learnosity.settings')->setData([
      'consumer_key' => $this->randomString(16),
      'consumer_secret' => $this->randomString(40),
    ])->save();

    // Set the field mappings for the node.
    $mappingHandler = \Drupal::service('learnosity.mappings_handler');
    $mappingHandler->setMappings('node', 'article', [
      'activity_id' => 'uuid',
      'name' => 'title',
    ]);
    $mappingHandler->setMappings('user', 'user', [
      'id' => 'uuid',
      'email' => 'mail',
    ]);
    $mappingHandler->save();

  }

}
