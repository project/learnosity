<?php

namespace Drupal\Tests\learnosity\Kernel;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\learnosity\Entity\LearnosityActivity;
use Drupal\node\Entity\Node;
use Drupal\Core\Form\FormState;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\learnosity\Entity\LearnosityActivityEditor;
use Drupal\Core\Entity\EntityInterface;

/**
 * Tests the authoring widget for users with different permissions.
 *
 * @group learnosity
 */
class LearnosityActivityAuthoringWidgetTest extends LearnosityKernelTestBase {

  use UserCreationTrait;

  /**
   * Disable schema validation when running tests.
   *
   * @var bool
   *
   * @todo: Fix this by providing actual schema validation.
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field_ui',
    'field',
    'user',
    'node',
    'user',
    'system',
    'field',
    'learnosity',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create article content type.
    $values = ['type' => 'article', 'name' => 'Article'];
    $node_type = NodeType::create($values);
    $node_type->save();

    // Add the learnosity_activity field to article.
    $field_name = 'field_learnosity';
    $field_storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'type' => 'learnosity_activity',
      'settings' => [
        'target_type' => 'learnosity_activity'
      ],
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'article',
      'settings' => [
        'editor' => [
          'basic' => 'basic',
          'default' => 'default',
          'read_only' => 0,
        ]
      ]
    ]);
    $field->save();

    // Create a new activity editor.
    $editor = LearnosityActivityEditor::create([
      'id' => 'basic',
    ]);
    $editor->save();
  }

  /**
   * Tests the behavior of a field component within an EntityFormDisplay object.
   */
  public function testUserWidgetAccess() {
    // TODO: A lot of this can be moved to a sub-module.
    $field_name = 'field_learnosity';

    // Check that the getWidget() method returns the correct widget plugin.
    $user = $this->createUser([
      'use learnosity activity editor basic',
    ], 'foobar');

    \Drupal::currentUser()->setAccount($user);

    // Create and store a Learnosity activity on the node.
    $uuid = '12345';
    $activity = LearnosityActivity::create([
      'reference' => $uuid,
    ]);
    $activity->save();

    $node = Node::create([
      'title' => $this->randomString(16),
      'type' => 'article',
      'field_learnosity' => $activity,
    ]);

    $element = $this->buildFieldWidgetElement($field_name, $node);

    $this->assertEquals('basic', $element['widget'][0]['target_id']['#editor']);

    // Check that the getWidget() method returns the correct widget plugin.
    $user = $this->createUser([
      'use learnosity activity editor default',
    ], 'foobar2');
    \Drupal::currentUser()->setAccount($user);

    $node = Node::create([
      'title' => $this->randomString(16),
      'type' => 'article',
      'field_learnosity' => $activity,
    ]);

    $element = $this->buildFieldWidgetElement($field_name, $node);

    $this->assertEquals('default', $element['widget'][0]['target_id']['#editor']);
  }

  /**
   * Build the field widget element.
   *
   * @param string $field_name
   *   The name of the field.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   *
   * @return mixed
   *   The field widget element.
   *
   * @throws \Drupal\Core\Form\EnforcedResponseException
   * @throws \Drupal\Core\Form\FormAjaxException
   */
  protected function buildFieldWidgetElement($field_name, EntityInterface $entity) {
    $form_display = EntityFormDisplay::create([
      'targetEntityType' => $entity->getEntityTypeId(),
      'bundle' => $entity->bundle(),
      'mode' => 'default',
    ]);
    $form_display->setComponent($field_name);
    $widget = $form_display->getRenderer($field_name);

    $items = $entity->get('field_learnosity');
    $items->filterEmptyItems();
    $form_state = (new FormState());
    $form_object = \Drupal::entityTypeManager()->getFormObject($entity->getEntityTypeId(), 'default');
    $form_object->setEntity($entity);
    $form = \Drupal::formBuilder()->buildForm($form_object, $form_state);
    return $widget->form($items, $form, $form_state);
  }

}
