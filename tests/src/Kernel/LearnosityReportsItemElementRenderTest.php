<?php

namespace Drupal\Tests\learnosity\Kernel;

use Drupal\learnosity\Element\LearnosityReportsItem;
use Drupal\Core\Render\Element;

/**
 * Tests the Learnosity Reports Item Render element.
 *
 * @group learnosity
 */
class LearnosityReportsItemElementRenderTest extends LearnosityKernelTestBase {

  /**
   * Disable schema validation when running tests.
   *
   * @var bool
   *
   * @todo: Fix this by providing actual schema validation.
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Tests the Learnosity Reports Item Render element.
   */
  public function testLearnosityReportsItemElement() {
    // This is a render element that is using Learnosity's API.
    // Because we can't test against Learnosity directly, we check some of
    // the important implementation details to ensure that it is set up as
    // expected.
    $element = [
      '#type' => 'learnosity_reports_item',
      '#id' => 'reports',
      '#reports' => [[
        'id' => 'sessions-summary',
        'type' => 'sessions-summary',
        'user_id' => '$ANONYMIZED_USER_ID',
        'session_ids' => [
          '8f8490d9-5ef7-4c59-bcdc-44df24202a12',
        ]],
      ],
    ];

    /** @var \Drupal\Core\Render\Renderer $renderer */
    $renderer = \Drupal::service('renderer');
    $renderer->renderRoot($element);

    $this->assertEquals('reports', $element['#attached']['drupalSettings']['learnosity']['service']);
    $this->assertTrue(!empty($element['#attached']['drupalSettings']['learnosity']['signedRequest']));

    // The element should always return the report as a child.
    $children = Element::children($element);
    $this->assertTrue(is_array($children));
  }

}
