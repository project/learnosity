<?php

namespace Drupal\Tests\learnosity\Kernel;

use Drupal\learnosity\Entity\LearnosityActivity;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\learnosity\Entity\LearnosityActivityEditor;
use Drupal\user\Entity\User;

/**
 * Tests the learnosity feature.
 *
 * @group learnosity
 */
class LearnosityFeatureTest extends LearnosityKernelTestBase {

  use UserCreationTrait;

  /**
   * Disable schema validation when running tests.
   *
   * @var bool
   *
   * @todo: Fix this by providing actual schema validation.
   */
  protected $strictConfigSchema = FALSE;

  /**
   * User for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $testUser;

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'user',
    'node',
    'system',
    'learnosity',
    'learnosity_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->testUser = User::create([
      'name' => 'foobar1',
      'mail' => 'foobar1@example.com',
    ]);
    $this->testUser->save();
    \Drupal::service('current_user')->setAccount($this->testUser);

    // Create a new activity editor.
    $editor = LearnosityActivityEditor::create([
      'id' => 'basic',
      'config' => '',
      'features' => [
        'test_feature' => 'test_feature',
      ]
    ]);
    $editor->save();
  }

  /**
   * Tests the initialization of the feature as part of the signed request.
   */
  public function testFeatureInitialization() {

    $entity = LearnosityActivity::create([
      'reference' => '123456',
    ]);

    $element = [
      '#type' => 'learnosity_activity_editor',
      // Leaving enabled in order for autocreate to work.
      '#target_type' => 'learnosity_activity',
      '#selection_handler' => 'default:learnosity',
      '#editor' => 'basic',
      '#selection_settings' => [],
      '#value' => ['reference' => $entity->getReference()],
      '#autocreate' => [
        'uid' => $this->testUser->id(),
      ],
      '#user' => [
        'id' => $this->testUser->id(),
        'email' => $this->testUser->getEmail(),
        'firstname' => $this->testUser->getAccountName(),
        'lastname' => 'testuser',
      ],
    ];

    /** @var \Drupal\Core\Render\Renderer $renderer */
    $renderer = \Drupal::service('renderer');
    $renderer->renderRoot($element);
    $request = json_decode($element['#attached']['drupalSettings']['learnosity']['signedRequest'], TRUE);

    $expected = [
      'custom_type' => 'test_feature',
      'name' => 'Feature Test',
      'js' => 'http://localhost/modules/contrib/learnosity/js/feature.test_feature.js',
      'css' => '',
      'version' => 'v0.0.1',
      'editor_layout' => 'http://localhost/modules/contrib/learnosity/templates/test_feature.html',
      'editor_schema' => [
        'description' => 'This is a feature test.',
        'hidden_question' => false,
        'properties' => [
          'text' => [
            'name' => 'Text field',
            'type' => 'string',
          ]
        ]
      ],
    ];
    $this->assertEquals($expected, $request['request']['config']['dependencies']['question_editor_api']['init_options']['custom_feature_types'][0]);

  }

}
