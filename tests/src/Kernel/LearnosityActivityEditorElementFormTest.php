<?php

namespace Drupal\Tests\learnosity\Kernel;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\learnosity\Entity\LearnosityActivity;
use Drupal\user\Entity\User;

/**
 * Tests the Learnosity Activity Editor Form API element.
 *
 * @group learnosity
 */
class LearnosityActivityEditorElementFormTest extends LearnosityKernelTestBase implements FormInterface {

  /**
   * Disable schema validation when running tests.
   *
   * @var bool
   *
   * @todo: Fix this by providing actual schema validation.
   */
  protected $strictConfigSchema = FALSE;

  /**
   * User for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $testUser;

  /**
   * An array of entities to be referenced in this test.
   *
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  protected $referencedEntities;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    \Drupal::service('router.builder')->rebuild();

    $this->testUser = User::create([
      'name' => 'foobar1',
      'mail' => 'foobar1@example.com',
    ]);
    $this->testUser->save();
    \Drupal::service('current_user')->setAccount($this->testUser);

    for ($i = 1; $i < 3; $i++) {
      $entity = LearnosityActivity::create([
        'reference' => $this->getActivityUuid(),
        'data' => 'a:2:{s:14:"rendering_type";s:6:"assess";s:6:"config";a:2:{s:10:"navigation";a:2:{s:10:"show_intro";b:1;s:10:"show_outro";b:1;}s:7:"regions";s:4:"main";}}',
      ]);
      $entity->save();
      $this->referencedEntities[] = $entity;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_learnosity_activity_editor';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['activity_blank'] = [
      '#type' => 'learnosity_activity_editor',
      // Leaving enabled in order for autocreate to work.
      '#target_type' => 'learnosity_activity',
      '#selection_handler' => 'default:learnosity',
      '#selection_settings' => [],
      '#autocreate' => [
        'uid' => $this->testUser->id(),
      ],
      '#user' => [
        'id' => $this->testUser->id(),
        'email' => $this->testUser->getEmail(),
        'firstname' => $this->testUser->getAccountName(),
        'lastname' => 'testuser',
      ],
    ];

    $form['activity_input'] = [
      '#type' => 'learnosity_activity_editor',
      // Leaving enabled in order for autocreate to work.
      '#target_type' => 'learnosity_activity',
      '#selection_handler' => 'default:learnosity',
      '#editor' => 'default',
      '#selection_settings' => [],
      '#autocreate' => [
        'uid' => $this->testUser->id(),
      ],
      '#user' => [
        'id' => $this->testUser->id(),
        'email' => $this->testUser->getEmail(),
        'firstname' => $this->testUser->getAccountName(),
        'lastname' => 'testuser',
      ],
    ];

    $form['activity_specific_entity'] = [
      '#type' => 'learnosity_activity_editor',
      // Leaving enabled in order for autocreate to work.
      '#target_type' => 'learnosity_activity',
      '#selection_handler' => 'default:learnosity',
      '#editor' => 'default',
      '#selection_settings' => [],
      '#default_value' => isset($this->referencedEntities[0]) ? $this->referencedEntities[0] : NULL,
      '#autocreate' => [
        'uid' => $this->testUser->id(),
      ],
      '#user' => [
        'id' => $this->testUser->id(),
        'email' => $this->testUser->getEmail(),
        'firstname' => $this->testUser->getAccountName(),
        'lastname' => 'testuser',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Tests the Learnosity Activity Editor Form API element.
   */
  public function testLearnosityActivityEditorElement() {
    // Setting submitted values.
    $form_state = (new FormState())
      ->setValues([
        'activity_input' => ['reference' => $this->referencedEntities[1]->getReference(), 'data' => urlencode($this->referencedEntities[1]->getData())],
      ]);
    $form_builder = $this->container->get('form_builder');
    $form_builder->submitForm($this, $form_state);

    // Valid form state.
    $this->assertCount(0, $form_state->getErrors());

    // Test the 'activity' element.
    $value = $form_state->getValue('activity_blank');
    // The UUID should match the following pattern.
    $this->assertMatchesRegularExpression('/[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/', $value['entity']->getReference());
    $this->assertEquals('learnosity_activity', $value['entity']->bundle());
    $this->assertEquals($this->testUser->id(), $value['entity']->getOwnerId());

    // Test the 'activity_input' element.
    $value = $form_state->getValue('activity_input');
    $this->assertEquals($this->referencedEntities[1]->getReference(), $value['entity']->getReference());
    $this->assertEquals('learnosity_activity', $value['entity']->bundle());
    $this->assertEquals($this->testUser->id(), $value['entity']->getOwnerId());

    // Test the 'activity_specific_entity' element.
    $value = $form_state->getValue('activity_specific_entity');
    $this->assertEquals($this->referencedEntities[0]->getReference(), $value['entity']->getReference());
    $this->assertEquals('learnosity_activity', $value['entity']->bundle());
    $this->assertEquals($this->testUser->id(), $value['entity']->getOwnerId());

  }

  /**
   * Returns a UUID generated from Learnosity's SDK service.
   *
   * @return string
   *   A UUID string.
   */
  protected function getActivityUuid() {
    return \Drupal::service('learnosity.sdk')->generateUuid();
  }

}
