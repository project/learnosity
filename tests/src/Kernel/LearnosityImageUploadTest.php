<?php

namespace Drupal\Tests\learnosity\Kernel;

use Drupal\learnosity\Controller\LearnosityImageUploadController;
use Drupal\learnosity\Entity\LearnosityActivity;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\learnosity\Entity\LearnosityActivityEditor;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Ajax\AjaxResponse;

/**
 * Tests the learnosity feature.
 *
 * @group learnosity
 */
class LearnosityImageUploadTest extends LearnosityKernelTestBase {

  use UserCreationTrait;

  /**
   * Disable schema validation when running tests.
   *
   * @var bool
   *
   * @todo: Fix this by providing actual schema validation.
   */
  protected $strictConfigSchema = FALSE;

  /**
   * User for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $testUser;

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'user',
    'node',
    'system',
    'learnosity',
    'learnosity_test',
    'media',
    'image',
    'file',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('file');
    $this->installEntitySchema('media');

    $this->testUser = User::create([
      'name' => 'foobar1',
      'mail' => 'foobar1@example.com',
    ]);
    $this->testUser->save();
    \Drupal::service('current_user')->setAccount($this->testUser);

    // Create a new activity editor.
    $editor = LearnosityActivityEditor::create([
      'id' => 'basic',
      'config' => '',
      'upload_widget' => [
        'plugin' => 'test_image_upload',
        'settings' => [
          'file_extension' => '.jpg'
        ],
      ]
    ]);
    $editor->save();
  }

  /**
   * Tests initialization of the Image Upload.
   */
  public function testImageUploadInitialization() {

    $entity = LearnosityActivity::create([
      'reference' => '123456',
    ]);

    $element = [
      '#type' => 'learnosity_activity_editor',
      // Leaving enabled in order for autocreate to work.
      '#target_type' => 'learnosity_activity',
      '#selection_handler' => 'default:learnosity',
      '#editor' => 'basic',
      '#selection_settings' => [],
      '#value' => ['reference' => $entity->getReference()],
      '#autocreate' => [
        'uid' => $this->testUser->id(),
      ],
      '#user' => [
        'id' => $this->testUser->id(),
        'email' => $this->testUser->getEmail(),
        'firstname' => $this->testUser->getAccountName(),
        'lastname' => 'testuser',
      ],
    ];

    /** @var \Drupal\Core\Render\Renderer $renderer */
    $renderer = \Drupal::service('renderer');
    $renderer->renderRoot($element);

    $this->assertEquals('test_image_upload', $element['#attached']['drupalSettings']['learnosity']['uploadWidget']);

  }

  /**
   * Tests the image upload ajax controller.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function testImageUploadControllerRequest() {
    $request_stack = new RequestStack();
    $request = new Request([
      'filename' => 'test_file',
      'target' => 'dom-element',
    ]);
    $request_stack->push($request);

    $pluginManager = \Drupal::service('plugin.manager.learnosity_image_upload');
    $learnosityEventController = new LearnosityImageUploadController($pluginManager);
    $response = $learnosityEventController->uploadForm($request, 'basic');

    $this->assertInstanceOf(AjaxResponse::class, $response);
  }

}
