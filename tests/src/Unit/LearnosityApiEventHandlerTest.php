<?php

namespace Drupal\Tests\learnosity\Unit;

use Drupal\learnosity\Event\LearnosityAssessmentEvent;
use Drupal\learnosity\LearnosityApiEventHandler;
use Drupal\Tests\UnitTestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Entity\EntityTypeEvents;

/**
 * @coversDefaultClass \Drupal\learnosity\LearnosityApiEventHandler
 * @group learnosity
 */
class LearnosityApiEventHandlerTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * Test getting subscribed events.
   */
  public function testGetSubscribedEvents() {
    $eventDispatcher = $this->prophesize(EventDispatcherInterface::class);
    $events = LearnosityAssessmentEvent::getEvents();
    foreach ($events as $name) {
      $eventDispatcher->hasListeners($name)
        ->willReturn(FALSE);
    }
    $eventDispatcher->hasListeners(LearnosityAssessmentEvent::FINISHED_SUBMIT)
      ->willReturn(TRUE);

    $learnosityApiEventHandler = new LearnosityApiEventHandler($eventDispatcher->reveal());
    $subscribedEvents = $learnosityApiEventHandler->getSubscribedEvents();

    $events = [
      'test:finished:submit',
    ];
    $this->assertSame($events, $subscribedEvents);

  }

  /**
   * Test the event name parser.
   */
  public function testParseEventName() {
    $eventDispatcher = $this->prophesize(EventDispatcherInterface::class);
    $learnosityApiEventHandler = new LearnosityApiEventHandler($eventDispatcher->reveal());
    $eventName = $learnosityApiEventHandler->parseEventName(LearnosityAssessmentEvent::FINISHED_SUBMIT);

    $this->assertEquals('test:finished:submit', $eventName);
  }

}
