<?php

namespace Drupal\Tests\learnosity\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\learnosity\Ajax\LearnosityImageUpdateCommand;

/**
 * Test coverage for learnosity ajax commands.
 *
 * @group learnosity
 */
class LearnosityAjaxCommandsTest extends UnitTestCase {

  /**
   * @covers \Drupal\learnosity\Ajax\LearnosityImageUpdateCommand
   */
  public function testImageUpdateCommand() {
    $command = new LearnosityImageUpdateCommand('#field-id', 'https://assets.learnosity.com/organisations/123/9c209fd9-776f-42ea-adc3-099425592f08.png');

    $expected = [
      'command' => 'learnosity_image_update',
      'target' => '#field-id',
      'value' => 'https://assets.learnosity.com/organisations/123/9c209fd9-776f-42ea-adc3-099425592f08.png',
    ];

    $this->assertEquals($expected, $command->render());
  }

}
