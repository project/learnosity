<?php

namespace Drupal\Tests\learnosity\Unit;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\learnosity\Controller\LearnosityEventController;
use Drupal\Tests\UnitTestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\learnosity\Event\LearnosityEvent;
use Prophecy\Argument;

/**
 * @coversDefaultClass \Drupal\learnosity\Controller\LearnosityEventController
 * @group learnosity
 */
class LearnosityEventControllerTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * Tests learnosity event controller.
   */
  public function testDispatchEvent() {

    $context = [
      'activity_id' => '1',
      'entity_type' => 'node',
      'entity_id' => '1',
      'session_id' => '1234567',
    ];
    $event = new LearnosityEvent('learnosity.test:finished:submit', $context);
    $response = new AjaxResponse();
    $event->setResponse($response);

    $eventDispatcher = $this->prophesize(EventDispatcherInterface::class);
    $eventDispatcher->dispatch(Argument::any(), Argument::any())
      ->willReturn($event)
      ->shouldBeCalled();

    $request_stack = new RequestStack();
    $request = new Request();
    $request_stack->push($request);

    $learnosityEventController = new LearnosityEventController($request_stack, $eventDispatcher->reveal());
    $response = $learnosityEventController->dispatchEvent('test-finished-submit');

    $this->assertInstanceOf(AjaxResponse::class, $response);
  }

}
