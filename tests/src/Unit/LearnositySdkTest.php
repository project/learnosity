<?php

namespace Drupal\Tests\learnosity\Unit;

use Drupal\learnosity\LearnositySdk;
use Drupal\Tests\UnitTestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * @coversDefaultClass \Drupal\learnosity\LearnositySdk
 * @group learnosity
 */
class LearnositySdkTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * The service container.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $container;

  /**
   * Request stack.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $request;

  /**
   * Configuration factory.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $config;

  /**
   * Module handler service.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $moduleHandler;

  /**
   * The logger service.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->container = $this->prophesize(ContainerInterface::class);
    $this->request = $this->prophesize(RequestStack::class);
    $this->config = $this->prophesize(ConfigFactoryInterface::class);
    $this->moduleHandler = $this->prophesize(ModuleHandlerInterface::class);
    $this->logger = $this->prophesize(LoggerChannelFactoryInterface::class);

    $settings = $this->prophesize(ConfigFactoryInterface::class);
    $settings->get('consumer_key')->willReturn('JPP3ahyyvLqnMGLG');
    $settings->get('consumer_secret')->willReturn('BvvTOasYkEe6thIVE0la1IilLolUa1hQ1kx5o12y');
    $this->config->get('learnosity.settings')->willReturn($settings->reveal());
  }

  /**
   * Testing initialization.
   */
  public function testInit() {
    $host = $this->prophesize(Request::class);
    $host->getHost()->willReturn('http://www.example.com');
    $host->reveal();
    $this->request->getCurrentRequest()->willReturn($host);

    $learnositySdk = new LearnositySdk($this->container->reveal(), $this->request->reveal(), $this->config->reveal(), $this->moduleHandler->reveal(), $this->logger->reveal());
    $request = [
      'activity_template_id' => 'activity_template_mock',
      'activity_id' => 'activity_mock',
      'name' => 'Mock activity',
      'rendering_type' => 'assess',
      'type' => 'local_practice',
      'session_id' => '12345',
      'user_id' => 1,
    ];
    $signedRequest = $learnositySdk->init('items', $request);

    // Learnosity will always return a json signed request with a signature.
    $json = json_decode($signedRequest, TRUE);
    $this->assertNotEmpty($json['security']['signature']);

  }

  /**
   * Test generating the API Library URL.
   */
  public function testgetApiLibraryUrl() {

    $this->container->getParameter('learnosity_api_version')->willReturn('v2021.2.LTS');
    $this->container->getParameter('learnosity_api_libraries')->willReturn([
      'authorapi' => 'https://authorapi.learnosity.com',
      'data' => 'https://data.learnosity.com',
    ]);

    $learnositySdk = new LearnositySdk($this->container->reveal(), $this->request->reveal(), $this->config->reveal(), $this->moduleHandler->reveal(), $this->logger->reveal());

    // Testing the standard API url structure.
    $url = $learnositySdk->getApiLibraryUrl('authorapi');
    $this->assertEquals('https://authorapi.learnosity.com/?v2021.2.LTS', $url);

    // Testing the data API url structure which is slightly different.
    $url = $learnositySdk->getApiLibraryUrl('data');
    $this->assertEquals('https://data.learnosity.com/v2021.2.LTS', $url);
  }

}
