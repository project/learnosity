<?php

namespace Drupal\learnosity_test\Plugin\LearnosityFeature;

use Drupal\learnosity\Plugin\LearnosityFeature\LearnosityFeatureBase;
use Drupal\learnosity\Plugin\LearnosityFeature\LearnosityFeatureInterface;

/**
 * A learnosity feature for testing purposes.
 *
 * @LearnosityFeature(
 *   id = "test_feature",
 *   label = "Feature Test",
 *   uuid = "9966b9a2-661a-4519-88ba-b0d5279e44fd",
 *   js = "/modules/contrib/learnosity/js/feature.test_feature.js",
 *   css = "",
 *   version = "v0.0.1",
 *   editor_layout = "/modules/contrib/learnosity/templates/test_feature.html",
 *   editor_schema = {
 *     "description" = "This is a feature test.",
 *     "hidden_question" = false,
 *     "properties" = {
 *       "text" = {
 *         "name" = "Text field",
 *         "type" = "string",
 *       },
 *     },
 *   },
 * )
 */
class TestFeature extends LearnosityFeatureBase implements LearnosityFeatureInterface {

}
