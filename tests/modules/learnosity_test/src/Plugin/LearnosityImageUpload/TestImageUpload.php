<?php

namespace Drupal\learnosity_test\Plugin\LearnosityImageUpload;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\learnosity\Plugin\LearnosityImageUpload\LearnosityImageUploadBase;
use GuzzleHttp\Psr7\Utils;
use Symfony\Component\HttpFoundation\Request;
use Drupal\image\Entity\ImageStyle;

/**
 * Defines an image upload plugin for testing purposes.
 *
 * @LearnosityImageUpload(
 *   id = "test_image_upload",
 *   label = "Test (Image)",
 *   entity_type = "media",
 *   bundle = "image",
 *   modal_settings = {
 *     "title" = "Title",
 *     "width" = 800,
 *   },
 * )
 */
class TestImageUpload extends LearnosityImageUploadBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'file_extension' => '.jpg',
        'rename' => FALSE,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(Request $request) {
    if (!$this->getSetting('rename')) {
      $value = $request->get('filename');
    }
    else {
      $value = 'file_renamed';
    }
    return $value . $this->getSetting('file_extension');
  }

}
