INTRODUCTION
------------

The Learnosity Integration module provides integration with Learnosity's
Assessment building system and Drupal.

 * To learn more about Learnosity, visit their website:\
 https://learnosity.com/

REQUIREMENTS
------------

There are no other required modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * Configure the connection information Administration > Configuration > Learnosity > Settings:

   - Configure the Consumer Key and Secret in order to connect to Learnosity.

 * Configure the mapping information from Administration > Configuration > Learnosity > Settings > Mapping

   - Use this to map user and entity fields to Learnosity. This is required for reporting.

 * Configure the Editors from Administration > Configuration > Learnosity > Learnosity editors:

   - Use this to configure the Learnosity activity authoring experience. This is very similar Drupal
   core text formats. Editors can be assigned on the field level as well as to different roles.

 * Configure the Players from Administration > Configuration > Learnosity > Learnosity players:

   - Use this to configure the Learnosity activity assessment experience. Specifically, this is what your
   students will see when they interact with Learnosity.
